.PHONY: build release install test clean

VERSION := 0.0.1
KROKI_CMD := kroki

build: hello object-graph

hello:
	$(KROKI_CMD) convert examples/hello.dot -t dot

sequence-diagram:
	$(KROKI_CMD) convert examples/sequence-diagram.blockdiag -t blockdiag

object-graph:
	$(KROKI_CMD) convert examples/object-graph.dot -t dot

entity-relationship:
	$(KROKI_CMD) convert examples/entity-relationship.erd -t erd

release: clean

install: 

test:

clean: