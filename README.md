# Watheia ⨈iz Service

> Visualize all the things!

![All the things](./docs/img/allthethings.jpg)

*WIP* This project is currently in alpha. The service works and is being used by us in production internally. We will release a 1.0 MVP  version once we finalize the API and all documentation is complete and up to date.

## Examples

### Hello, World

![Hello World Example](./examples/hello.svg)

### Sequence Diagram

![Sequence Diagram](./examples/sequence-diagram.svg)

### Object Graph

![Object Graph](./examples/object-graph.svg)

### Entity Relationship

![Entity Relationship](./examples/entity-relationship.svg)

### Sketch

![UI Mockup](./examples/wireframe.svg)

## Word Cloud

![Word Cloud](./examples/word-cloud.svg)

### Chart

![Chart](./examples/chart.svg)

### UML

![UML](./examples/uml.svg)

### Work Breakdown Structure

![WBS](./examples/wbs.svg)

### Gantt Chart

![Gantt](./examples/gantt.svg)

### Business Process

![Gantt](./examples/bpnm.svg)

### Timing

![Gantt](./examples/timing.svg)

### Deployment (sketch)

![Gantt](./examples/deployment.svg)

### Context (C4)

![Context](./examples/context.svg)

### Container (C4)

![Container](./examples/container.svg)

### Component (C4)

![Component](./examples/component.svg)

### State Machine

![Component](./examples/statemachine.svg)

## References

- <https://docs.kroki.io/kroki/>
- <https://plantuml.com/>
